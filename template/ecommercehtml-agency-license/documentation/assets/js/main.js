// ========= Pricing range slider one
var stepsSlider = document.getElementById('slider-range');
var input0 = document.getElementById('minAmount');
var input1 = document.getElementById('maxAmount');
var inputs = [input0, input1];
noUiSlider.create(stepsSlider, {
    start: [200, 1200],
    connect: true,
    step: 1,
    range: {
        'min': [0],
        'max': 2000
    },

});

stepsSlider.noUiSlider.on('update', function (values, handle) {
    inputs[handle].value = values[handle];
});