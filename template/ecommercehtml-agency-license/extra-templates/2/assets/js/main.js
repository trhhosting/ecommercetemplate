
// ========= Preloader
window.onload = function () {
  window.setTimeout(fadeout, 500);
}

function fadeout() {
  document.querySelector('.preloader').style.opacity = '0';
  document.querySelector('.preloader').style.display = 'none';
}

// ========= main-menu 4  activation
let menuToggle4 = document.querySelector('.menu-style-4 .menu-toggle');
menuToggle4.addEventListener('click', () => {
  menuToggle4.classList.toggle('active');
})

//======== tiny slider for header-5-active
tns({
  autoplay: true,
  autoplayButtonOutput: false,
  mouseDrag: true,
  gutter: 0,
  container: ".header-5-active",
  center: true,
  nav: true,
  controls: false,
  speed: 400,
  controlsText: [
      '<i class="lni lni-arrow-left-circle"></i>',
      '<i class="lni lni-arrow-right-circle"></i>',
  ],
  responsive: {
      0: {
          items: 1,
      },
  }
});

//======== tiny slider for preview
tns({
  autoplay: true,
  autoplayButtonOutput: false,
  mouseDrag: true,
  gutter: 0,
  nav: true,
  controls: true,
  controlsText: [
      '<i class="mdi mdi-chevron-left"></i>',
      '<i class="mdi mdi-chevron-right"></i>',
  ],
  "container": "#customize",
  "items": 1,
  "center": true,
  "navContainer": "#customize-thumbs",
  "navAsThumbnails": true,
  "autoplayTimeout": 5000,
  "swipeAngle": false,
  "speed": 400,
});

//====== counter up 
var cu = new counterUp({
  start: 0,
  duration: 2000,
  intvalues: true,
  interval: 100,
  append: " ",
});
cu.start();