
// ========= Preloader
window.onload = function () {
  window.setTimeout(fadeout, 500);
}

function fadeout() {
  document.querySelector('.preloader').style.opacity = '0';
  document.querySelector('.preloader').style.display = 'none';
}


// ========= main-menu 12  activation
let mobileMenuOpen12 = document.querySelector('.mobile-menu-open-12');
let mobileMenuClose12 = document.querySelector('.close-mobile-menu-12');
let navbarSidebar12 = document.querySelector('.navbar-sidebar-12');
let overlay12 = document.querySelector('.overlay-12');
mobileMenuOpen12.addEventListener('click', () => {
  navbarSidebar12.classList.add('open');
  overlay12.classList.add('open')
})
mobileMenuClose12.addEventListener('click', () => {
  navbarSidebar12.classList.remove('open');
  overlay12.classList.remove('open')
})
overlay12.addEventListener('click', () => {
  navbarSidebar12.classList.remove('open');
  overlay12.classList.remove('open')
})


//======== tiny slider for content-card-style-7
tns({
  autoplay: true,
  autoplayButtonOutput: false,
  mouseDrag: true,
  gutter: 0,
  container: ".content-card-style-7",
  center: true,
  nav: true,
  controls: false,
  speed: 400,
  controlsText: [
      '<i class="lni lni-arrow-left-circle"></i>',
      '<i class="lni lni-arrow-right-circle"></i>',
  ],
  responsive: {
      0: {
          items: 1,
      },
  }
});

// =========== select-item-4 active
selectItem4=document.querySelectorAll("#select-item-4 .single-item");
for(var i=0; i<selectItem4.length; i++){
    selectItem4[i].onclick=function(){
    var el=selectItem4[0];
    while(el){
        if(el.tagName==="DIV"){
        el.classList.remove("active");
        }
        el=el.nextSibling;
    }
    this.classList.add("active");
    };
}

// =========== select-color-4 active
selectColor4=document.querySelectorAll("#select-color-4 li");
for(var i=0; i<selectColor4.length; i++){
    selectColor4[i].onclick=function(){
    var el=selectColor4[0];
    while(el){
        if(el.tagName==="LI"){
        el.classList.remove("active");
        }
        el=el.nextSibling;
    }
    this.classList.add("active");
    };
}