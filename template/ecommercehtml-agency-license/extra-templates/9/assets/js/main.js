
// ========= Preloader
window.onload = function () {
  window.setTimeout(fadeout, 500);
}

function fadeout() {
  document.querySelector('.preloader').style.opacity = '0';
  document.querySelector('.preloader').style.display = 'none';
}

// ========= main-menu-dark 4  activation
let menuToggleDark4 = document.querySelector('.menu-dark-4 .menu-toggle');
menuToggleDark4.addEventListener('click', () => {
  menuToggleDark4.classList.toggle('active');
})

//======== tiny slider for header-7-active
tns({
  autoplay: true,
  autoplayButtonOutput: false,
  mouseDrag: true,
  gutter: 0,
  container: ".header-7-active",
  center: true,
  nav: true,
  controls: false,
  speed: 400,
  controlsText: [
      '<i class="lni lni-arrow-left-circle"></i>',
      '<i class="lni lni-arrow-right-circle"></i>',
  ],
  responsive: {
      0: {
          items: 1,
      },
  }
});

// =========== select-item-6 active
selectItem6=document.querySelectorAll("#select-item-6 .single-item");
for(var i=0; i<selectItem6.length; i++){
    selectItem6[i].onclick=function(){
    var el=selectItem6[0];
    while(el){
        if(el.tagName==="DIV"){
        el.classList.remove("active");
        }
        el=el.nextSibling;
    }
    this.classList.add("active");
    };
}

// =========== select-color-6 active
selectColor6=document.querySelectorAll("#select-color-6 li");
for(var i=0; i<selectColor6.length; i++){
    selectColor6[i].onclick=function(){
    var el=selectColor6[0];
    while(el){
        if(el.tagName==="LI"){
        el.classList.remove("active");
        }
        el=el.nextSibling;
    }
    this.classList.add("active");
    };
}