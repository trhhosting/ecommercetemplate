      switch (sliderCount) {
        case 0:
          Element slider = querySelector('#slider');

          Element slider0 = querySelector('#${products[0].productData[0].sku}');
          slider0.hidden = false;
          Element slider1 = querySelector('#${products[1].productData[0].sku}');
          slider1.hidden = true;
          Element slider2 = querySelector('#${products[2].productData[0].sku}');
          slider2.hidden = true;
          break;
        case 1:
          Element slider0 = querySelector('#${products[0].productData[0].sku}');
          slider0.hidden = true;
          Element slider1 = querySelector('#${products[1].productData[0].sku}');
          slider1.hidden = false;
          Element slider2 = querySelector('#${products[2].productData[0].sku}');
          slider2.hidden = true;
          break;
        case 2:
          Element slider0 = querySelector('#${products[0].productData[0].sku}');
          slider0.hidden = true;
          Element slider1 = querySelector('#${products[1].productData[0].sku}');
          slider1.hidden = true;
          Element slider2 = querySelector('#${products[2].productData[0].sku}');
          slider2.hidden = false;
          break;
      }
      sliderCount++;
      if (sliderCount > 2) {
        sliderCount = 0;
      }