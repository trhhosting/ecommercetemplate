// @dart=2.11
// ignore_for_file: unsafe_html

import 'dart:html';
import 'dart:convert';
import 'package:apt/src/comm.dart';
import 'package:apt/src/decodePayload.dart';
import 'package:apt/insert.dart';
import 'package:apt/src/models/messageEadd.dart';
import 'package:apt/src/models/products.dart';
import 'package:apt/src/models/policy.dart';
import 'package:apt/src/models/index.dart';
import 'dart:math';

var gcxval = StoreHtmlValidator();

String productCategory = '';
Future<void> main() async {
  // ImageElement x = querySelector('#heroicon');
  // reade the url
  String url = Uri.parse(window.location.href).host.toString();
  var params = Uri.parse(window.location.href).queryParameters;
  //excape string for url
  productCategory = base64Url.encode("1-4 Luggage & Travel Bags".codeUnits);
  params.forEach((key, value) {
    if (key == 'category') {
      productCategory = value;
    }
    print('$key: $value');
    print(productCategory);
  });
  print(url);
  Head head = Head.fromJson(jsonDecode(await getHead()));
  headUpdateDisplay(head, url);
  productCategory = base64Url.encode(head.niche.codeUnits);
  Element supportEmail = querySelector('#support_email');
  supportEmail.text = head.email;
  supportEmail.setAttribute('href', 'mailto:' + head.email);
  supportEmail.setAttribute('class', 'mdi mdi-email');
  if (window.innerWidth < 500) {
    supportEmail.hidden = true;
  }
  Element supportPhone = querySelector('#support_phone');
  supportPhone.text = head.phone;
  supportPhone.setAttribute('href', 'tel:' + head.phone);
  Element supportAddress = querySelector('#menu_address');
  supportAddress.text = " " + head.address;
  bool onAbout = false;

  // get data from JS wall.js
  bool msgPolices = false;
  document.on["message"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    MessageWall msg = MessageWall.fromJson(json.decode(dataConvert));
    if (msg.name == "policies") {
      Element policy = querySelector('#policy');
      if (msgPolices) {
        print("policies true");
        // get the policy
        mainDisplay(true);
        policyDisplay(false);
        navMobileDisplay(false);
        aboutUsDisplay(false);
        msgPolices = false;
        policy.focus();
      } else {
        print("policies false");
        mainDisplay(false);
        policyDisplay(true);
        msgPolices = true;
      }
    }
    if (msg.name == "home") {
      print("went home");
      // get the policy
      mainDisplay(true);
      policyDisplay(false);
      navMobileDisplay(false);
      aboutUsDisplay(false);
      window.scrollTo(0, 0);
    }
    if (msg.name == "loggleNavMobile") {
      print("loggleNavMobile");
      Element nav = querySelector('#nav_mobile');
      if (nav.hidden) {
        navMobileDisplay(true);
      } else {
        navMobileDisplay(false);
      }
    }
    if (msg.name == "about") {
      print("about");
      Element nav = querySelector('#about_us');
      if (!onAbout) {
        aboutUsDisplayHTML(head);
        onAbout = true;
      }
      if (nav.hidden) {
        aboutUsDisplay(true);
        mainDisplay(false);
      } else {
        mainDisplay(true);
        aboutUsDisplay(false);
      }
    }
    if (msg.name == "policy_donotsell") {
      print("policy_donotsell");
      Element nav = querySelector('#policy_donotsell');
      if (nav.hidden) {
        policiesDisplay(true, "policy_donotsell");
      } else {
        policiesDisplay(false, "policy_donotsell");
      }
    }
    if (msg.name == "policy_privacy") {
      print("policy_privacy");
      Element nav = querySelector('#policy_privacy');
      if (nav.hidden) {
        policiesDisplay(true, "policy_privacy");
      } else {
        policiesDisplay(false, "policy_privacy");
      }
    }
    if (msg.name == "policy_shipping") {
      print("policy_shipping");
      Element nav = querySelector('#policy_shipping');
      if (nav.hidden) {
        policiesDisplay(true, "policy_shipping");
      } else {
        policiesDisplay(false, "policy_shipping");
      }
    }
    if (msg.name == "policy_refund") {
      print("policy_refund");
      Element nav = querySelector('#policy_refund');
      if (nav.hidden) {
        policiesDisplay(true, "policy_refund");
      } else {
        policiesDisplay(false, "policy_refund");
      }
    }
  });
  document.on["sendSms"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    print(dataConvert);
    sendSmsMarketing(dataConvert);
  });
  List<Product> products = await loadProducts(head);
  List<Product> cart = <Product>[];

  document.on["addToCart"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    // print(dataConvert);
    ProductSku productSku = ProductSku.fromJson(json.decode(dataConvert));
    Product prd = addProductFromCart(productSku.sku, products);
    cart.add(addProductFromCart(productSku.sku, products));
    window.alert('Added to cart! \n${prd.name}');
    try {
      cartPreview(cart);
      cartPreviewDisplay(true);
    } catch (e) {
      print("cart empty");
    }
  });
  document.on["removeFromCart"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    // print(dataConvert);
    ProductSku productSku = ProductSku.fromJson(json.decode(dataConvert));
    List<Product> cartTmp = (delProductFromCart(productSku.sku, cart));
    cart.clear();
    cart.addAll(cartTmp);
    try {
      cartPreview(cart);
      cartView(cart);
      cartPreviewDisplay(true);
    } catch (e) {
      print("cart empty");
    }
  });
  document.on["checkOut"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    print(dataConvert);
    mainDisplay(false);
    policyDisplay(false);
    cartView(cart);
    cartDisplay(true);
  });
  document.on["continueShopping"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    print(dataConvert);
    mainDisplay(true);
    cartDisplay(false);
  });
  document.on["viewMore"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    ProductView productView = ProductView.fromJson(json.decode(dataConvert));
    viewMore(productView.productId);
  });
  int loopCount = 0;
  int sliderCount = 0;
  document.on["gameLoop"].listen((Event event) async {
    // var data = (event as CustomEvent).detail;
    // var dataConvert = json.encode(data);
    // print(dataConvert);
    loopCount++;

    if (loopCount % 5 == 0) {
      switch (sliderCount) {
        case 0:
          for (int i = 0; i < 3; i++) {
            try {
              if (i == 0) {
                querySelector(
                        '#slider_${products[i].productData[0].sku.replaceAll(" ", "_")}')
                    .hidden = false;
              } else {
                querySelector(
                        '#slider_${products[i].productData[0].sku.replaceAll(" ", "_")}')
                    .hidden = true;
              }
            } catch (e) {
              print("sliderView error");
            }
          }

          break;
        case 1:
          for (int i = 0; i < 3; i++) {
            try {
              if (i == 1) {
                querySelector(
                        '#slider_${products[i].productData[0].sku.replaceAll(" ", "_")}')
                    .hidden = false;
              } else {
                querySelector(
                        '#slider_${products[i].productData[0].sku.replaceAll(" ", "_")}')
                    .hidden = true;
              }
            } catch (e) {
              print("sliderView error");
            }
          }
          break;
        case 2:
          for (int i = 0; i < 3; i++) {
            try {
              if (i == 2) {
                querySelector(
                        '#slider_${products[i].productData[0].sku.replaceAll(" ", "_")}')
                    .hidden = false;
              } else {
                querySelector(
                        '#slider_${products[i].productData[0].sku.replaceAll(" ", "_")}')
                    .hidden = true;
              }
            } catch (e) {
              print("sliderView error");
            }
          }
          break;
      }
      sliderCount++;
      if (sliderCount > 2) {
        sliderCount = 0;
      }
    }
    Element landing0 = Element.div();

    if (loopCount == 1) {
      landingPageView(products, head.callToAction);
      var rnd = Random();
      landing0 = querySelector(
          '#landing_${products[rnd.nextInt(products.length - 1)].productData[0].sku.replaceAll(" ", "_")}');
      landing0.hidden = false;
      landingDisplay(true);
    } else if (loopCount == 11) {
      navTopBarDisplay(true);
      mainDisplay(true);
      landingDisplay(false);
      footerDisplay(true);
    }

    if (loopCount > 100) {
      products = await loadProducts(head);
      loopCount = 12;
    }
  });
  policy(url);
  // navTopBarDisplay(true);
  // mainDisplay(true);
  // searchDisplay(true);
  // cartDisplay(false);
  footerDisplay(true);
}

Future<List<Product>> loadProducts(Head head) async {
  footerView(head);
  List<Product> products = await ProductListingService(productCategory);
  List<Product> productHeadliner = <Product>[];
  List<Product> productFeatured = <Product>[];
  List<Product> productHotItems = <Product>[];
  List<Product> productDisplayItems = <Product>[];
  List<Product> productListing = <Product>[];
  int count = 0;
  products.forEach((product) {
    if (count <= 3) {
      productHeadliner.add(product);
    } else if (count > 3 && count <= 6) {
      productFeatured.add(product);
    } else if (count > 6 && count <= 10) {
      productHotItems.add(product);
    } else if (count > 10 && count <= 12) {
      productDisplayItems.add(product);
    } else {
      productListing.add(product);
    }
    count++;
  });
  String productHeadlinerHtml = '';
  String productFeaturedHtml = '';
  String productHotItemsHtml = '';
  String productDisplayHtml = '';
  String productListingHtml = '';
  int productHeadlinerCount = 0;
  productHeadliner.forEach((product) {
    if (productHeadlinerHtml == '') {
      productHeadlinerHtml = '<div  class="content-card-7-wrapper">';
    }
    productHeadlinerHtml += '<div class="content-card-style-7 content-active">';
    if (productHeadlinerCount == 2) {
      productHeadlinerHtml = productHeadlinerHtml +
          '''
    <div id="slider_${product.productData[0].sku.replaceAll(" ", "_")}" class="single-content-card bg_cover" style="background-image: url(${product.mainImage});">
      <div class="content-card">
          <h4 class="title">
            ${product.name}
          </h4>
        
          <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" /> \$ ${product.productData[0].price} <br> Add to Cart</button>
          
      </div>
  </div>

''';
    } else {
      productHeadlinerHtml = productHeadlinerHtml +
          '''
    <div hidden id="slider_${product.productData[0].sku.replaceAll(" ", "_")}" class="single-content-card bg_cover" style="background-image: url(${product.mainImage});">
      <div class="content-card">
          <h4 class="title">
            ${product.name}
          </h4>
        
          <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" /> \$ ${product.productData[0].price} <br> Add to Cart</button>
          
      </div>
  </div>

''';
    }
    productHeadlinerCount++;
  });
  productHeadlinerHtml = productHeadlinerHtml + '</div>';
  Element productHeadlinerElement = querySelector('#headliner_slider');
  productHeadlinerElement.setInnerHtml(productHeadlinerHtml,
      validator: gcxval.vali);

  productFeatured.forEach((product) {
    if (productFeaturedHtml == '') {
      productFeaturedHtml = '''
            <div class="col-lg-4 col-md-6">
              <div class="product-style-17 mt-30">
                <div class="product-image">
                  <img
                    src="${product.mainImage}"
                    alt="${product.productProperty}"
                  />
                </div>
                <div class="product-content text-center">
                  <h4 class="title">${product.name}</h4>
                  <br>
                  <h3 >\$ ${product.productData[0].price}</h3>
                  <span class="price">
                  <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add to Cart</button>
                  </span>
                </div>
              </div>
            </div>
''';
    } else {
      productFeaturedHtml = productFeaturedHtml +
          ''' 
            <div class="col-lg-4 col-md-6">
              <div class="product-style-17 mt-30">
                <div class="product-image">
                  <img
                    src="${product.mainImage}"
                    alt="${product.productProperty}"
                  />
                </div>
                <div class="product-content text-center">
                  <h4 class="title">${product.name}</h4>
                <br>
                  <h3 class="sub-title center-text">\$ ${product.productData[0].price}</h3>
                  <span class="price">
                  <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add to Cart</button></span>

                </div>
              </div>
            </div>
      ''';
    }
  });
  Element productFeaturedElement = querySelector('#product_featured');
  productFeaturedElement.setInnerHtml(productFeaturedHtml,
      validator: gcxval.vali);
  shoppingInfoHTML(products);
  productHotItems.forEach((product) {
    if (productHotItemsHtml == "") {
      productHotItemsHtml = '''
      <div class="col-lg-3 col-sm-6">
          <div class="product-style-11 mt-30">
              <div class="product-image">
                  <div class="product-item">
                  <img
                    src="${product.mainImage}"
                    alt="${product.productProperty}"
                  />
                  </div>
                  <a class="add-wishlist" onClick="selectFav('${product.productData[0].sku}')">
                      <i class="mdi mdi-heart-outline"></i>
                  </a>
              </div>
              <div class="product-content">
                  <span class="rating"><i class="mdi mdi-star"></i> 4.5/5</span>
                  <h6 class="title">${product.name}</h6>
                  <br>
                <div class="product-content text-center">
              	<h3 class="price">\$ ${product.productData[0].price}</h3>
                  <span class="price">
                  <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add To Cart</button>
                  </span>
              </div>

                  <p>
                  ${product.description.substring(0, getWordCount(product.description))} 
                  <div id="product_${product.productData[0].sku}" hidden>
                  ${product.description.substring(getWordCount(product.description), product.description.length)}
                  </div>
                  <button onClick="viewMore('${product.productData[0].sku}')" class="main-btn"> ... more .. </button>
                  </p>
                  <span hidden class="price">\$ ${product.productData[0].price}
                  <button hidden onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" /> Add to Cart</button>
                  </span>
 
                  
              </div>
          </div>
      </div>
''';
    } else {
      productHotItemsHtml = productHotItemsHtml +
          '''
      <div class="col-lg-3 col-sm-6">
          <div class="product-style-11 mt-30">
              <div class="product-image">
                  <div class="product-item">
                  <img
                    src="${product.mainImage}"
                    alt="${product.productProperty}"
                  />
                  </div>
                  <a class="add-wishlist" onClick="selectFav('${product.productData[0].sku}')">
                      <i class="mdi mdi-heart-outline"></i>
                  </a>
              </div>
              <div class="product-content">
                  <span class="rating"><i class="mdi mdi-star"></i> 4.5/5</span>
                  <h6 class="title">${product.name}</h6>
                  <br>
              <div class="product-content text-center">
              	<h3 class="price">\$ ${product.productData[0].price}</h3>
                  <span class="price">
                  <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add To Cart</button>
                  </span>
              </div>

                  <p>
                  ${product.description.substring(0, getWordCount(product.description))} 
                  <div id="product_${product.productData[0].sku}" hidden>
                  ${product.description.substring(getWordCount(product.description), product.description.length)}
                  </div>
                  <button onClick="viewMore('${product.productData[0].sku}')" class="main-btn"> ... more .. </button>
                  </p>
                  <span hidden class="price"> \$ ${product.productData[0].price}
                  <button hidden onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add to Cart</button>
                  </span>
              </div>
          </div>
      </div>''';
    }
  });

  Element productHotItemsElement = querySelector('#product_hot_items');
  productHotItemsElement.setInnerHtml(productHotItemsHtml,
      validator: gcxval.vali);
  if (productDisplayItems.isNotEmpty) {
    productDisplayHtml = '''
            <div class="content-card-style-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="content-card-content">
                                <h3 class="sub-title">
                                ${productDisplayItems[0].name}
                                </h3>
                                <h2 class="title">
                                <h3 class="price">\$ ${productDisplayItems[0].productData[0].price}</h3>
                                </h2>
                                <p class="title">
                                ${productDisplayItems[1].description.substring(0, getWordCount(productDisplayItems[1].description))} 
                                <div class="title" id="product_${productDisplayItems[0].productData[0].sku}" hidden>
                                ${productDisplayItems[0].description}
                                </div>
                                <button onClick="viewMore('${productDisplayItems[0].productData[0].sku}')" class="main-btn"> ... more .. </button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-card-image bg_cover" style="background-image: url(${productDisplayItems[0].mainImage});">
              </div>
                  <div class="product-content text-center">
              	
                  <span class="price">
                  <button onClick="addToCart('${productDisplayItems[0].productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add To Cart</button>
                  </span>

                </div>
            </div>
            <div class="content-card-style-1">
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-md-6">
                            <div class="content-card-content">
                                <h3 class="sub-title">
                                ${productDisplayItems[1].name}
                                </h3>
                                <h2 class="title">
                                <br>
                                <h3 class="price">\$ ${productDisplayItems[1].productData[0].price}</h3>
                                </h2>

                                <p class="title">
                                ${productDisplayItems[1].description.substring(0, getWordCount(productDisplayItems[1].description))} 
                                <div class="title" id="product_${productDisplayItems[1].productData[0].sku}" hidden>
                                ${productDisplayItems[1].description}
                                </div>
                                <button onClick="viewMore('${productDisplayItems[1].productData[0].sku}')" class="main-btn"> ... more .. </button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="product-content">
                  <span class="price">
                  <button onClick="addToCart('${productDisplayItems[1].productData[0].sku}')" class="main-btn primary-btn btn-theme"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />Add To Cart</button>
                  </span>
              </div>
           <div class="content-card-image-2 bg_cover" style="background-image: url( ${productDisplayItems[1].mainImage});">
                </div>
                </div>
            </div>
''';
  }

  Element productDisplayElement = querySelector('#product_display');
  productDisplayElement.setInnerHtml(productDisplayHtml,
      validator: gcxval.vali);
  int productListingCount = 0;
  try {
    productListing.forEach((product) {
      if (productListingCount == 0) {
        productListingHtml = '''
      <div class="col-lg-4 col-md-6">
          <div class="product-style-19 mt-30">
              <div class="product-content text-center">
                  <h4 class="title"><a onClick="addToCart('${product.productData[0].sku}')">${product.name}</a></h4>
                  <p>
                  ${product.description.substring(1, 250)}}
                  </p>
              </div>
              <div class="product-image">
                  <img
                    src="${product.mainImage}"
                    alt="${product.productProperty}"
                  />
              </div>
              <div class="product-content text-center">
                  <span class="price"> 
                  <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn price"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />${product.productData[0].price}</button>
                  </span>
              </div>

          </div>
      </div>
      ''';
      } else {
        productListingHtml = productListingHtml +
            '''
      <div class="col-lg-4 col-md-6">
          <div class="product-style-19 mt-30">
              <div class="product-content text-center">
                  <h4 class="title"><a onClick="addToCart('${product.productData[0].sku}')">${product.name}</a></h4>
                  <p>
                  ${product.description.substring(1, (product.description.length * 0.5).toInt())}
                  </p>
              </div>
              <div class="product-image">
                  <img
                    src="${product.mainImage}"
                    alt="${product.productProperty}"
                  />
              </div>
              <div class="product-content text-center">
                  <span class="price"> 
                  <button onClick="addToCart('${product.productData[0].sku}')" class="main-btn primary-btn price"> <img src="/assets/images/icon-svg/cart-7.svg" alt="" />${product.productData[0].price}</button>
                  </span>
              </div>
          </div>
      </div>
      ''';
      }
      ;
      if (productListingCount >= 8) {
        throw 'Product Listing Count Exceeded';
      }
      productListingCount++;
    });
  } catch (e) {
    print(e);
  }

  Element productListingElement = querySelector('#product_listing');
  productListingElement.setInnerHtml(productListingHtml,
      validator: gcxval.vali);
  return products;
}

void navTopBarDisplay(bool active) {
  if (active) {
    querySelector('#nav_top_bar').hidden = false;
  } else {
    querySelector('#nav_top_bar').hidden = true;
  }
}

void landingDisplay(bool active) {
  if (active) {
    querySelector('#landing').hidden = false;
  } else {
    querySelector('#landing').hidden = true;
  }
}

void mainDisplay(bool active) {
  if (active) {
    querySelector('#main').hidden = false;
  } else {
    querySelector('#main').hidden = true;
  }
}

void searchDisplay(bool active) {
  if (active) {
    querySelector('#search').hidden = false;
  } else {
    querySelector('#search').hidden = true;
  }
}

void cartDisplay(bool active) {
  if (active) {
    querySelector('#cart').hidden = false;
  } else {
    querySelector('#cart').hidden = true;
  }
}

void cartPreviewDisplay(bool active) {
  if (active) {
    querySelector('#cart_preview').hidden = false;
  } else {
    querySelector('#cart_preview').hidden = true;
  }
}

void footerDisplay(bool active) {
  if (active) {
    querySelector('#footer').hidden = false;
  } else {
    querySelector('#footer').hidden = true;
  }
}

void policyDisplay(bool active) {
  if (active) {
    querySelector('#policy').hidden = false;
  } else {
    querySelector('#policy').hidden = true;
  }
}

void aboutUsDisplay(bool active) {
  if (active) {
    querySelector('#about_us').hidden = false;
  } else {
    querySelector('#about_us').hidden = true;
  }
}

void navMobileDisplay(bool active) {
  if (active) {
    querySelector('#nav_mobile').hidden = false;
  } else {
    querySelector('#nav_mobile').hidden = true;
  }
}

void policiesDisplay(bool active, String policy) {
  if (active) {
    querySelector('#$policy').hidden = false;
  } else {
    querySelector('#$policy').hidden = true;
  }
}

void viewMore(String sku) {
  DivElement tmp = querySelector('#product_$sku');
  tmp.hidden = !tmp.hidden;
  // if (tmp.hidden) {
  //   querySelector('#$sku').hidden = false;
  // } else {
  //   querySelector('#$sku').hidden = true;
  // }
}

Product addProductFromCart(String productID, List<Product> products) {
  return products
      .firstWhere((product) => product.productData[0].sku == productID);
}

List<Product> delProductFromCart(String productID, List<Product> cart) {
  return cart
      .where((product) => product.productData[0].sku != productID)
      .toList();
}

void cartPreview(List<Product> cart) {
  String cartPreviewHtml = '''
<div class="checkout-header d-flex justify-content-between">
    <h6 class="title">Shopping Cart</h6>
</div>

<div class="checkout-table">
        <div class="checkout-btn">
            <button hidden onClick="continueShopping()" class="main-btn primary-btn-border">Continue Shopping</button>
            <button onClick="checkOut()" class="main-btn primary-btn btn-theme">Checkout</button>
        </div>

    <table class="table table-theme">
        <tbody>
''';
  double cartTotal = 0.00;
  cart.forEach((Product product) {
    cartTotal = cartTotal + product.productData[0].price;
    cartPreviewHtml = cartPreviewHtml +
        '''            
    <tr>
        <td class="checkout-product">
            <div class="product-cart d-flex">
                <div class="product-thumb">
                    <img src="${product.mainImage}" alt="${product.name}" />
                </div>
                <div class="product-content media-body">
                    <h5 class="title">
                        ${product.name}
                    </h5>
                    <ul>
                        <li><span>${product.productData[0].specifications}</span></li>
                        <li>
                            <button onClick="delProductFromCart('${product.productData[0].sku}')" class="delete" href="#0"><i class="mdi mdi-delete"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
        </td>
        <td hidden class="checkout-quantity">
            <div class="product-quantity d-inline-flex">
                <button type="button" id="sub" class="sub">
                    <i class="mdi mdi-minus"></i>
                </button>
                <input type="text" value="1" />
                <button type="button" id="add" class="add">
                    <i class="mdi mdi-plus"></i>
                </button>
            </div>
        </td>
        <td class="checkout-price">
            <p class="price">\$ ${product.productData[0].price}</p>
        </td>
    </tr>

    ''';
  });
  cartPreviewHtml = cartPreviewHtml +
      '''
</tbody>
        </table>
    </div>

    <div class="checkout-footer">
        <div class="checkout-sub-total d-flex justify-content-between">
            <p class="value">Subtotal Price:</p>
            <p class="price">\$ ${cartTotal.toStringAsPrecision(3)}</p>
        </div>
    </div>
''';
  Element cartPreviewElement = querySelector('#cart_preview');
  cartPreviewElement.setInnerHtml(cartPreviewHtml, validator: gcxval.vali);
  Element cartBadge = querySelector('#cart_badge');
  cartBadge.setInnerHtml(cart.length.toString(), validator: gcxval.vali);
  Element cartMobileBadge = querySelector('#mobile_cart_badge');
  cartMobileBadge.setInnerHtml(cart.length.toString(), validator: gcxval.vali);
}

void cartView(List<Product> cart) {
  String cartHtml = '''
  <div class="col-lg-12">
    <div class="checkout-style-1">
      <div class="checkout-table table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th class="product">Product</th>
              <th hidden class="quantity">Quantity</th>
              <th class="price">Price</th>
              <th class="action">Action</th>
            </tr>
          </thead>
          <tbody>
  ''';

  double cartTotal = 0.00;
  cart.forEach((Product product) {
    cartTotal = cartTotal + product.productData[0].price;
    cartHtml = cartHtml +
        '''
  <tr>
    <td>
      <div class="product-cart d-flex">
        <div class="product-thumb">
          <img height="45vh" src="${product.mainImage}" alt="${product.name}" />
        </div>
        <div class="product-content media-body">
          <h5 class="title"><a href="#0">${product.name}</a></h5>
          <span>${product.productData[0].specifications}</span>
        </div>
      </div>
    </td>
    <td hidden>
      <div class="product-quantity d-inline-flex">
        <button type="button" id="sub" class="sub">
          <i class="mdi mdi-minus"></i>
        </button>
        <input type="text" value="0" />
        <button type="button" id="add" class="add">
          <i class="mdi mdi-plus"></i>
        </button>
      </div>
    </td>
    <td>
      <p class="price">\$${product.productData[0].price}</p>
    </td>
    <td>
      <ul class="action">
          <button onClick="delProductFromCart('${product.productData[0].sku}')" class="delete" href="#0"><i class="mdi mdi-delete"></i></button>
      </ul>
    </td>
  </tr>
  ''';
  });
  cartHtml = cartHtml +
      '''
              </tbody>
                  </table>
                </div>
                <div
                  class="
                    checkout-coupon-total checkout-coupon-total-2
                    d-flex
                    flex-wrap
                  "
                  >
                  <div class="checkout-coupon">
                    <span>Apply Coupon to get discount!</span>
                    <form action="#">
                      <div class="single-form form-default d-flex">
                        <div class="form-input form">
                          <input type="text" placeholder="Coupon Code" />
                        </div>
                        <button class="main-btn primary-btn btn-theme">Apply</button>
                      </div>
                    </form>
                  </div>
                  <div class="checkout-total">
                    <div class="single-total">
                      <p class="value">Subtotal Price:</p>
                      <p class="price">\$ ${cartTotal.toStringAsPrecision(3)}</p>
                    </div>
                    <div class="single-total">
                      <p class="value">Shipping Cost (+):</p>
                      <p class="price">\$ <s>${(cartTotal * 0.45).toStringAsPrecision(3)}</s></p>
                    </div>
                    <div class="single-total total-payable">
                      <p class="value">Tax Payable:</p>
                      <p class="price">\$ ${(cartTotal * 0.06).toStringAsPrecision(3)}</p>
                    </div>
                    <div class="single-total total-payable">
                      <p class="value">Total Payable:</p>
                      <p class="price">\$ ${(cartTotal * 1.06).toStringAsPrecision(3)}</p>
                    </div>
                  </div>
                </div>
                <div class="checkout-btn d-sm-flex justify-content-between">
                  <div class="single-btn">
                    <button onClick="continueShopping()" class="main-btn primary-btn-border btn-theme">
                    continue shopping
                    </button>
                  </div>
                  <div class="single-btn">
                    <button onClick="payNow()" class="main-btn primary-btn btn-theme">Pay now</button>
                  </div>
                </div>
              </div>
            </div>

  ''';
  Element cartElement = querySelector('#cart');
  cartElement.setInnerHtml(cartHtml, validator: gcxval.vali);
}

void landingPageView(List<Product> products, String callToAction) {
  String landingPageHtml = '''
    <section class="cta-style-1 pt-100" style="background-image: url('${products[0].imageList[products[0].imageList.length - 1]}'); background-size: cover;">
  ''';
  products.forEach((Product product) {
    landingPageHtml = landingPageHtml +
        '''
      <div hidden id="landing_${product.productData[0].sku}" class="container">
        <div class="row">
            <div class="col-lg-12">
              <div
              class="
                alert alert-primary-light alert-dismissible
                fade
                show
              "
              role="alert"
            >
              <figure class="d-flex align-content-center">
                <i class="mdi mdi-clock-alert"></i>
                <figcaption class="media-body">
                  $callToAction
                </figcaption>
              </figure>
            </div>

            </div>
          </div>
        <div class="cta-style-1-wrapper secondary-gradient-1">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <div class="cta-content">
                <h1 class="mb-20 text-white">Save \$100 dollars today</h1>
                <p class="mb-15 text-white">
                Would you like to save 100 dollars today? Well, then sign up for our newsletter to receive exclusive offers and discounts. Our deals are emailed to you monthly, and they're always tailored to your specific interests. So what are you waiting for? Sign up now and start saving!
                </p>
                <form class="has-validation-callback">
                  <div class="single-form text-black">
                    <div class="form-input">
                      <input type="text" placeholder="Your Email Please" />
                      <i class="mdi mdi-email"></i>
                    </div>
                    <br>
                    <button id="countdown" onclick="messageWall('emailSignUp')" class="main-btn primary-btn align-content-end">Subscribe</button>
                  </div>
                </form>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="cta-img">
                <img src="${product.imageList[0]}" alt="" />
              </div>
            </div>
            <br><br><br>
          </div>
          <br><br><br>
          <div class="row align-items-center">

      
          </div>

        </div>
      </div>

    ''';
  });
  landingPageHtml = landingPageHtml +
      '''
  </section>
  ''';
  Element landingPageElement = querySelector('#landing');
  landingPageElement.setInnerHtml(landingPageHtml, validator: gcxval.vali);
}

Future<String> getCompany(String policyName, String url) async {
  Company company = Company();
  CompanyInfo companyInfo = CompanyInfo();
  companyInfo.domain = url;
  companyInfo.name = url.split('.').first;
  company.companyInfo = companyInfo;
  company.policyFile = policyName;
  String data = jsonEncode(company.toJson());
  String donotsell = await policyTerms(data);
  // base64Decode(donotsell);
  return utf8.decode(base64Url.decode(donotsell));
}

void policy(String url) async {
  String donotsell = await getCompany("donotsell", url);
  String privacy = await getCompany("privacypolicy", url);
  String refund = await getCompany("refund_policy", url);
  String shipping = await getCompany("shippping_policy", url);
  String messageWallHtml = '''
    <!--====== Feature Style 2 Part Start ======-->

    <section class="pb-40 feature-wrapper mt-50 pt-70 bg-white-variant">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-8 col-sm-10">
            <div class="feature-box feature-style-2">
              <div class="feature-icon bg-caution-color">
                <i class="lni lni-code"></i>
              </div>
              <div class="feature-content">
                <h3>Do Not Sell My Information</h3>
                <p>
                ${donotsell.substring(0, getPolicyWordCount(donotsell))}
                </p>
                <div class="title" id="policy_donotsell" hidden>
                <p class="title">
                $donotsell
                </p>
                </div>
                <button onClick="messageWall('policy_donotsell')" class="main-btn"> ... more .. </button>

              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-8 col-sm-10">
            <div class="feature-box feature-style-2">
              <div class="feature-icon bg-secondary-1-color">
                <i class="lni lni-layers"></i>
              </div>
              <div class="feature-content">
                <h3>Privacy Policy</h3>
                <p id="policy_privacy_preview">
                ${privacy.substring(0, getPolicyWordCount(privacy))}
                </p>
                <div class="title" id="policy_privacy" hidden>
                <p class="title">
                $privacy
                </p>
                </div>
                <button onClick="messageWall('policy_privacy')" class="main-btn"> ... more .. </button>

              </div>

            </div>
          </div>
          <div class="col-lg-4 col-md-8 col-sm-10">
            <div class="feature-box feature-style-2">
              <div class="feature-icon bg-error-color">
                <i class="lni lni-cog"></i>
              </div>
              <div class="feature-content">
                <h3>Refund Policy</h3>
                <p id="policy_refund_preview">
                ${refund.substring(0, getPolicyWordCount(refund))}
                </p>
                <div class="title" id="policy_refund" hidden>
                <p class="title">
                $refund
                </p>
                </div>
                <button onClick="messageWall('policy_refund')" class="main-btn"> ... more .. </button>

              </div>

            </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-8 col-sm-10">
            <div class="feature-box feature-style-2">
              <div class="feature-icon bg-caution-color">
                <i class="lni lni-code"></i>
              </div>
              <div class="feature-content">
                <h3>Shipping Policy</h3>
                <p id="policy_shipping_preview">
                ${shipping.substring(0, getPolicyWordCount(shipping))}
                </p>
                <div class="title" id="policy_shipping" hidden>
                <p class="title">
                $shipping
                </p>
                </div>
                <button onClick="messageWall('policy_shipping')" class="main-btn"> ... more .. </button>

              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-8 col-sm-10">
            <div class="feature-box feature-style-2" hidden>
              <div class="feature-icon bg-secondary-1-color">
                <i class="lni lni-layers"></i>
              </div>
              <div class="feature-content">
                <h3></h3>
                <p>
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-8 col-sm-10">
            <div class="feature-box feature-style-2" hidden>
              <div class="feature-icon bg-error-color">
                <i class="lni lni-cog"></i>
              </div>
              <div class="feature-content">
                <h3></h3>
                <p>
                </p>
              </div>
            </div>
        </div>        
        </div>
      </div>
    </section>

    <!--====== Feature Style 2 Part Ends ======-->
  ''';
  Element policyElement = querySelector('#policy');
  policyElement.setInnerHtml(messageWallHtml, validator: gcxval.vali);
}

void footerView(Head head) async {
  String footerHtml = '''
        <!--====== Footer Style 2 Part Start ======-->

      <section class="footer-style-2 mt-50 pt-50 pb-100">
        <div class="container">
          <div class="footer-top">
            <div class="footer-top-left mt-50">
              <a href="">
                <img src="/static/icons/favicon.svg" height="100vh" alt="" />
              </a>
            </div>
            <div class="footer-top-right mt-50">
              <span class="title">We Accept:</span>
              <img src="assets/images/payment.png" alt="" />
            </div>
          </div>
  

  
          <div class="footer-bottom">
            <div class="footer-copyright">
              <p>Powered by &copy; <a href="https://github.com/trhhosting">TRH Hosting</a>,  All rights reserved.
              </p>
            </div>
            <div class="footer-follow">
              <span class="title">Follow Us:</span>
              <ul class="social-follow">
                <li>
                  <a href=""><i class="lni lni-facebook-filled"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-twitter-filled"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-linkedin-original"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-instagram-original"></i></a>
                </li>
                <li>
                  <a href=""><i class="lni lni-whatsapp"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
  
      <!--====== Footer Style 2 Part Ends ======-->

  ''';
  Element policyElement = querySelector('#footer');
  policyElement.setInnerHtml(footerHtml, validator: gcxval.vali);
}

void headUpdateDisplay(Head head, String url) {
  String headHtml = '''
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title id="title">${head.slogan} | ${head.company}</title>
      <link rel="apple-touch-icon" sizes="180x180" href="/static/icons/apple-touch-icon.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/static/icons/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/static/icons/favicon-16x16.png" />
      <link rel="manifest" href="/static/icons/site.webmanifest" />
      <link rel="mask-icon" href="/static/icons/safari-pinned-tab.svg" color="#5bbad5" />
      <link rel="shortcut icon" href="/static/icons/favicon.ico" />
      <meta name="msapplication-TileColor" content="#da532c" />
      <meta name="msapplication-config" content="/static/icons/browserconfig.xml" />
      <meta name="theme-color" content="f0f0f" />
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="apple-mobile-web-app-title" content="${head.title}">
      <meta name="application-name" content="${head.company}">
      <meta property="og:title" content="${head.title}" />
      <meta property="og:type" content="website" />
      <meta property="og:url" content="https://$url" />
      <meta property="og:image" content="${head.cardImage}" />
      <meta property="og:description" content="${head.title}" />
      <meta name="twitter:card" content="summary">
      <meta name="twitter:url" content="https://$url">
      <meta name="twitter:title" content="${head.title}">
      <meta name="twitter:description" content="${head.slogan}">
      <meta name="twitter:image" content="${head.cardImage}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
  
      <link rel="stylesheet" type="text/css" href="/styles.css" />
  ''';
  Element htmlElment = querySelector('#head');
  htmlElment.setInnerHtml(headHtml, validator: gcxval.vali);
}

// change this informaion for each site you create
void shoppingInfoHTML(List<Product> products) {
  var rnd = Random();
  Product product = products[rnd.nextInt(products.length)];
  String shoppingInfoHtml = '''
    <!--====== Call To Action style 5 Part Start ======-->

    <section
      class="cta-style-5 mt-70 pb-100 pt-100"
      style="background-image: url(${product.imageList[rnd.nextInt(product.imageList.length)]});"
    >
      <div class="container">
        <div class="cta-wrapper">
          <div class="row">
            <div class="col-lg-6">
              <div class="cta-content">
                <h2>
                  <strong>Subscribe Our Newsletter</strong>
                </h2>
                <h3>get Complimentary shipping on all orders over \$50 and get 20% off your first purchase</h3>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="cta-form-wrapper">
                <form class="has-validation-callback">
                  <div class="single-form form-default form-border">
                    <div class="form-input">
                      <input id="email_2" type="text" placeholder="Enter Your Email" />
                      <i class="mdi mdi-person"></i>
                      <input id="full_name_2" type="text" placeholder="Enter Your Full Name" />
                      <i class="mdi mdi-email"></i>
                      <button onClick="getEmailAddress();" class="main-btn primary-btn">Subscribe</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="back-to-top">
      <i class="lni lni-chevron-up"></i>
      <br />
    </div>

    <!--====== Call To Action style 5 Part Ends ======-->

  ''';
  Element shoppingInfoElement = querySelector('#shopping_info');
  shoppingInfoElement.setInnerHtml(shoppingInfoHtml, validator: gcxval.vali);
}

void aboutUsDisplayHTML(Head about) {
  String aboutUsHtml = '''
    <!--====== About Us Part Start ======-->

    <section class="about-us pt-100 pb-100">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="about-us-content">
              <h2 class="title">
                <strong>${about.about.title}</strong>
              </h2>
              <p>${about.about.text}</p>
              <a href="/about" class="main-btn primary-btn">Read More</a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="about-us-image">
              <img src="${about.about.image}" alt="About Us" />
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--====== About Us Part Ends ======-->

  ''';
  Element aboutUsElement = querySelector('#about_us');
  aboutUsElement.setInnerHtml(aboutUsHtml, validator: gcxval.vali);
}

int getWordCount(String s) {
  int length = s.length;
  if (length < 40) {
    return length;
  }
  if ((length <= 100) && (length >= 40)) {
    return length * 0.5.toInt();
  }
  if ((length <= 200) && (length >= 100)) {
    return length * 0.25.toInt();
  }
  if ((length <= 500) && (length >= 200)) {
    return length * 0.15.toInt();
  }
  return length * 0.1.toInt();
}

int getPolicyWordCount(String s) {
  int length = s.length;
  // print(length);
  return 200;
}
