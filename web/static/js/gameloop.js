var lastRender = 0
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
 }
const workClock = async () => {
    await sleep(1000)
        gameLoop()
        workClock()
    }
