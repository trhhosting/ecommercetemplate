function dispatchAddCartEvent(productID) {
    var event = new CustomEvent("addCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
        "price_id": priceID,
        "usage_type": usageType,

      }
    });

    document.dispatchEvent(event);
    alert(productName +" has been added to your Cart.");
}
function dispatchDelCartEvent(productName, productID, price, priceID, usageType) {
    var event = new CustomEvent("delCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
        "price_id": priceID,
        "usage_type": usageType,
      }
    });
    document.dispatchEvent(event);
    alert(productName +" has been removed to your Cart.");
}
function checkoutEvent(type, price) {
    var event = new CustomEvent("checkoutCart", {
      detail: {
        "payment_type": type,
        "price": price,
      }
    });
    document.dispatchEvent(event);
}
function sendSmsEvent() {
  if (document.getElementById('phone').value == "") {
    alert("Please enter your email address");
    return;
  }
    var event = new CustomEvent("sendSms", {
      detail: {
        "phone": document.getElementById('phone').value,
        "notes": document.getElementById('notes').value,
      }
    });
    document.dispatchEvent(event);
    document.getElementById('phone').value = "";
    document.getElementById('notes').value = "";
    alert("Thank you for your message. We put you in the loop and get back to you shortly.");
}

function getEmailAddress() {
    var event = new CustomEvent("getEmailAddress", {
      detail: {
        "email": document.getElementById('email').value,
        "full_name": document.getElementById('full_name').value,
        "message": document.getElementById('message').value,
      }
    });
    document.dispatchEvent(event);
    alert("Thank you for your message. We will get back to you shortly.");
}
function addToCart(sku){
    var event = new CustomEvent("addToCart", {
      detail: {
        "sku": sku,
      }
    });
    document.dispatchEvent(event);
    // console.log(sku);
    // alert("Product has been added to your Cart.");
}
function delProductFromCart(sku){
    var event = new CustomEvent("removeFromCart", {
      detail: {
        "sku": sku,
      }
    });
    document.dispatchEvent(event);
    // console.log(sku);
    //alert("Product has been removed from your Cart.");
}
function checkOut(){
    var event = new CustomEvent("checkOut", {
      detail: {
        "name": "checkOut",
      }
    });
    document.dispatchEvent(event);
    // console.log(sku);
    // alert("Product has been removed from your Cart.");
}
function continueShopping(){
    var event = new CustomEvent("continueShopping", {
      detail: {
        "name": "continueShopping",
      }
    });
    document.dispatchEvent(event);
    // console.log(sku);
    // alert("Product has been removed from your Cart.");
}
function viewMore(productID){
    var event = new CustomEvent("viewMore", {
      detail: {
        "product_id": productID,
      }
    });
    document.dispatchEvent(event);
    // console.log(sku);
    // alert("Product has been removed from your Cart.");
}

function gameLoop(){
  var event = new CustomEvent("gameLoop", {
    detail: {
      "name": "gameLoop",
    }
  });
  document.dispatchEvent(event);
  // console.log(sku);
  // alert("Product has been removed from your Cart.");
}
function messageWall(message){
  var event = new CustomEvent("message", {
    detail: {
      "name": message,
    }
  });
  document.dispatchEvent(event);
  // console.log(sku);
  // alert("Product has been removed from your Cart.");
}
function getEmailAddress() {
  var event = new CustomEvent("getEmailAddress", {
    detail: {
      "email": document.getElementById('email_2').value,
      "full_name": document.getElementById('full_name_2').value,
    }
  });
  document.dispatchEvent(event);
  alert("Thank you for your message. We will get back to you shortly.");
  document.getElementById('email_2').value = "";
  document.getElementById('full_name_2').value = "";
}