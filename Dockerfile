# 
FROM quay.io/spivegin/gitonly:latest AS git
FROM quay.io/spivegin/caddy_only:latest AS caddy
FROM spivegin/ecommerceapi:latest AS api

FROM dart:2.17.5 AS build
WORKDIR /opt/gcx
RUN ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa && git config --global user.name "quadtone" && git config --global user.email "quadtone@txtsme.com"
COPY --from=git /root/.ssh /root/.ssh
ENV deploy=c1f18aefcb3d1074d5166520dbf4ac8d2e85bf41 

RUN ssh-keyscan -H github.com > ~/.ssh/known_hosts &&\
    ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts &&\
    ssh-keyscan -H gitea.com >> ~/.ssh/know_hosts &&\
    ssh-keyscan -H api.smsburr.com >> ~/.ssh/know_hosts

RUN git config --global url.git@github.com:.insteadOf https://github.com/ &&\
    git config --global url.git@gitlab.com:.insteadOf https://gitlab.com/ &&\
    git config --global url.git@gitea.com:.insteadOf https://gitea.com/ &&\
    git config --global url."https://${deploy}@sc.tpnfc.us/".insteadOf "https://sc.tpnfc.us/"

ADD . /opt/gcx/
RUN rm -rf /root/.pub-cache/
RUN dart pub get &&\
    dart run build_runner build -o web:release --release --delete-conflicting-outputs 

FROM quay.io/spivegin/tlmbasedebian
RUN mkdir /opt/bin
WORKDIR /opt/gcx
COPY --from=caddy /bin/caddy /bin/caddy
COPY --from=api /opt/bin/ecomapi /bin/ecomapi
COPY --from=build /opt/gcx/release /opt/gcx/release
ADD ./Caddyfile.deploy /opt/gcx/Caddyfile
ADD ./files/bash/start.sh /opt/bin/start.sh
RUN chmod +x /opt/bin/start.sh
ADD ./files/bash/deb/dumb-init_1.2.5_amd64.deb /opt/gcx/dumb-init_1.2.5_amd64.deb
RUN dpkg -i dumb-init_*.deb
EXPOSE 8080
CMD ["/opt/bin/start.sh"]