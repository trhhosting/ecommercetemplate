import 'dart:html';

import 'package:htmlcomp/src/validators.dart';

class _ItemUrlPolicy implements UriPolicy {
  RegExp regex = RegExp(r'(?:https://)?.*');
  @override
  bool allowsUri(String uri) {
    return regex.hasMatch(uri);
  }
}

class StoreHtmlValidator {
  NodeValidatorBuilder validator = NodeValidatorBuilder()
    ..allowTextElements()
    ..allowTemplating()
    ..allowHtml5(uriPolicy: _ItemUrlPolicy())
    ..allowNavigation(_ItemUrlPolicy())
    ..allowImages(_ItemUrlPolicy())
    ..allowElement('a', attributes: ['onclick'])
    ..allowElement('input', attributes: ['style'])
    ..allowElement('div', attributes: ['style', 'onclick'])
    ..allowElement('button', attributes: ['style', 'onclick'])
    ..allowElement('img', attributes: ['style'])
    ..allowElement('head', attributes: ['style'])
    ..allowElement('body', attributes: ['style', 'onclick'])
    ..allowSvg();
  GodNodeValidator _vali = GodNodeValidator();
  StoreHtmlValidator();
  NodeValidatorBuilder get v => validator;

  //vali accept everything
  GodNodeValidator get vali => this._vali;
  set vali(GodNodeValidator vali) {
    this._vali = vali;
  }
}
