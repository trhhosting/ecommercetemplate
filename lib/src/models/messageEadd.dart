class MessageWall {
  String name;

  MessageWall({this.name});

  MessageWall.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this.name;
    return data;
  }
}
