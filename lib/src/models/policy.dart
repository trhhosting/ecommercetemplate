class Company {
  CompanyInfo companyInfo;
  String policyFile;

  Company({this.companyInfo, this.policyFile});

  Company.fromJson(Map<String, dynamic> json) {
    companyInfo = json['company_info'] != null
        ? new CompanyInfo.fromJson(json['company_info'])
        : null;
    policyFile = json['policy_file'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.companyInfo != null) {
      data['company_info'] = this.companyInfo.toJson();
    }
    data['policy_file'] = this.policyFile;
    return data;
  }
}

class CompanyInfo {
  String name;
  String domain;

  CompanyInfo({this.name, this.domain});

  CompanyInfo.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    domain = json['domain'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this.name;
    data['domain'] = this.domain;
    return data;
  }
}
