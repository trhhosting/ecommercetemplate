class Head {
  String company;
  String niche;
  String title;
  String phone;
  String email;
  String slogan;
  String callToAction;
  String cardImage;
  String address;
  String logo;
  Social social;
  About about;

  Head(
      {this.company,
      this.niche,
      this.title,
      this.phone,
      this.email,
      this.slogan,
      this.callToAction,
      this.cardImage,
      this.address,
      this.logo,
      this.social,
      this.about});

  Head.fromJson(Map<String, dynamic> json) {
    company = json['company'];
    niche = json['niche'];
    title = json['title'];
    phone = json['phone'];
    email = json['email'];
    slogan = json['slogan'];
    callToAction = json['callToAction'];
    cardImage = json['cardImage'];
    address = json['address'];
    logo = json['logo'];
    social = json['social'] = new Social.fromJson(json['social']);
    about = json['about'] = new About.fromJson(json['about']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['company'] = this.company;
    data['niche'] = this.niche;
    data['title'] = this.title;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['slogan'] = this.slogan;
    data['callToAction'] = this.callToAction;
    data['cardImage'] = this.cardImage;
    data['address'] = this.address;
    data['logo'] = this.logo;
    if (this.social != null) {
      data['social'] = this.social.toJson();
    }
    if (this.about != null) {
      data['about'] = this.about.toJson();
    }
    return data;
  }
}

class Social {
  String facebook;
  String twitter;
  String instagram;
  String pinterest;
  String youtube;

  Social(
      {this.facebook,
      this.twitter,
      this.instagram,
      this.pinterest,
      this.youtube});

  Social.fromJson(Map<String, dynamic> json) {
    facebook = json['facebook'];
    twitter = json['twitter'];
    instagram = json['instagram'];
    pinterest = json['pinterest'];
    youtube = json['youtube'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['facebook'] = this.facebook;
    data['twitter'] = this.twitter;
    data['instagram'] = this.instagram;
    data['pinterest'] = this.pinterest;
    data['youtube'] = this.youtube;
    return data;
  }
}

class About {
  String title;
  String text;
  String image;

  About({this.title, this.text, this.image});

  About.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    text = json['text'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = this.title;
    data['text'] = this.text;
    data['image'] = this.image;
    return data;
  }
}
