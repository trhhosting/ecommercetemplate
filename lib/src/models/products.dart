import 'dart:convert';

class Category {
  String name;
  String id;

  Category({this.name, this.id});

  Category.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

List<Category> categoryFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Category>.from(data.map((item) => Category.fromJson(item)));
}

class Product {
  String name;
  List<String> imageList;
  String mainImage;
  List<ProductData> productData;
  List<String> productProperty;
  String description;
  Product(
      {this.name,
      this.imageList,
      this.mainImage,
      this.productData,
      this.productProperty,
      this.description});

  Product.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    imageList = json['imageList'].cast<String>();
    mainImage = json['mainImage'];
    productData = productDataFromJson(json['productData']);
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this.name;
    data['imageList'] = this.imageList;
    data['mainImage'] = this.mainImage;
    if (this.productData != null) {
      data['productData'] = this.productData;
    }
    data['productProperty'] = this.productProperty;
    return data;
  }
}

class ProductData {
  String sTANDARD;
  String commodityDifferenceOption;
  String id;
  String img;
  double packWeight;
  String parentID;
  double price;
  String sku;
  List<String> specifications;
  List<String> varietyCommodityDifferenceOption;
  double weight;

  ProductData(
      {this.sTANDARD,
      this.commodityDifferenceOption,
      this.id,
      this.img,
      this.packWeight,
      this.parentID,
      this.price,
      this.sku,
      this.specifications,
      this.varietyCommodityDifferenceOption,
      this.weight});

  ProductData.fromJson(Map<String, dynamic> json) {
    sTANDARD = json['STANDARD'];
    commodityDifferenceOption = json['commodityDifferenceOption'];
    id = json['id'];
    img = json['img'];
    packWeight = json['packWeight'];
    parentID = json['parentID'];
    price = json['price'];
    sku = json['sku'];
    specifications = json['specifications'].cast<String>();
    varietyCommodityDifferenceOption =
        json['varietyCommodityDifferenceOption'].cast<String>();
    weight = json['weight'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['STANDARD'] = this.sTANDARD;
    data['commodityDifferenceOption'] = this.commodityDifferenceOption;
    data['id'] = this.id;
    data['img'] = this.img;
    data['packWeight'] = this.packWeight;
    data['parentID'] = this.parentID;
    data['price'] = this.price;
    data['sku'] = this.sku;
    data['specifications'] = this.specifications;
    data['varietyCommodityDifferenceOption'] =
        this.varietyCommodityDifferenceOption;
    data['weight'] = this.weight;
    return data;
  }
}

List<Product> productFromJson(String jsonData) {
  List<dynamic> data = json.decode(jsonData);

  return data.map((item) => Product.fromJson(item)).toList();
}

List<ProductData> productDataFromJson(List dynamicList) {
  List<ProductData> productDataList = [];
  dynamicList.forEach((item) {
    productDataList.add(ProductData.fromJson(item));
  });

  return productDataList;
}

class ProductSku {
  String sku;

  ProductSku({this.sku});

  ProductSku.fromJson(Map<String, dynamic> json) {
    sku = json['sku'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['sku'] = this.sku;
    return data;
  }
}

class ProductView {
  String productId;

  ProductView({this.productId});

  ProductView.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['product_id'] = this.productId;
    return data;
  }
}
