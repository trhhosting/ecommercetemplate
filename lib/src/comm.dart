import 'package:http_client/browser.dart';
import 'package:http/http.dart' as http;
import 'dart:html';

Future<String> dataServices() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/services.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> messageServices() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/message.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> timezones() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/timezones.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> hoursOfOperation() async {
  final client = BrowserClient();
  final rs =
      await client.send(Request('GET', '/static/data/hoursofoperation.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> getCalendarDisplayData() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/calendar.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> getAppSettings() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/app.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> getHours() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/hours.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> getHead() async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/static/data/env.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

void sendJsonStart(String data) async {
  String domain = "";
  domain = window.location.href;
  domain = domain.replaceAll(window.location.protocol, "");
  domain = domain.replaceAll("/", "");
  domain = domain.replaceAll("#", "_");
  Uri url = Uri.parse('/api/data/application/start/$domain');
  // var url = '/api/data/application/start/$domain';
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(url, headers: headers, body: data);
  print(response.body);
}

void sendAuthCalendar(String data) async {
  String domain = "";
  domain = window.location.href;
  domain = domain.replaceAll(window.location.protocol, "");
  domain = domain.replaceAll("/", "");
  domain = domain.replaceAll("#", "_");
  var url = '/api/v1/calendar/auth/';
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(Uri.parse(url), headers: headers, body: data);
  print(response.body);
}

Future<String> getAvailability(dynamic data) async {
  print(data);
  var url = '/api/v1/calendar/schedule';
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(Uri.parse(url), headers: headers, body: data);
  // print(response.body);
  return response.body.toString();
}

void sendAppointmentJsonEventSet(String data) async {
  var url = '/api/v1/calendar/appointment/';
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  await http.post(Uri.parse(url), headers: headers, body: data);
}

void addNewProduct(String data) async {
  var url = '/api/v1/product/add/';
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  await http.post(Uri.parse(url), headers: headers, body: data);
}

void sendSmsMarketing(String data) async {
  Uri url = Uri.parse('/api/v1/sms/marketing/');
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  await http.post(url, headers: headers, body: data);
}

Future<String> policyTerms(String data) async {
  Uri url = Uri.parse('/api/v1/policy/');
  var headers = <String, String>{};
  headers = {'Content-Type': 'application/json'};
  // await http.post(url,headers: headers, body: data);
  var response = await http.post(url, headers: headers, body: data);
  return response.body.toString();
}
