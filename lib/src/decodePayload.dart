import 'package:http_client/browser.dart';
import 'package:apt/src/models/products.dart';

Future<String> _dataServices(String productCategory) async {
  final client = BrowserClient();
  final rs =
      await client.send(Request('GET', '/api/v1/products/$productCategory'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<List<Product>> ProductListingService(String productCategory) async {
  String products = await _dataServices(productCategory);
  return productFromJson(products);
}
